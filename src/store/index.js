import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

axios.defaults.baseURL = process.env.VUE_APP_API_URL
const API_KEY = process.env.VUE_APP_API_KEY

export default new Vuex.Store({
  state: {
    images: []
  },

  mutations: {
    SET_IMAGES (state, payload) {
      state.images = payload
    }
  },

  actions: {
    getImage ({ state, dispatch }, payload) {
      if (!state.images.length) {
        return dispatch('retrieveImageById', payload)
      }

      return state.images.find(item => item.id === payload)
    },

    getImages ({ commit, state }) {
      if (!state.images.length) {
        return axios.get('', {
          params: {
            key: API_KEY,
            q: 'cats',
            image_type: 'all',
            per_page: 100
          }
        })
          .then(({ data }) => {
            commit('SET_IMAGES', data.hits)
            return data.hits
          })
          .catch((error) => {
            throw error
          })
      }

      return state.images
    },

    retrieveImageById (context, id) {
      return axios.get('', {
        params: {
          key: API_KEY,
          id: id
        }
      })
        .then(({ data }) => {
          return data.hits[0]
        })
        .catch((error) => {
          throw error
        })
    }
  },

  modules: {
  }
})
